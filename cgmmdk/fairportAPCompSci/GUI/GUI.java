/*The Chris Grace CheckList (subject to change, if you have ideas)
 * text field for equation 	DONE
 * button to add more text fields for equations 	INCOMPLETE
 * Preview (if technically feasible) 	INCOMPLETE
 * button to confirm and start making the GIF 	DONE
 * check box to upload to gyfcat	NYI
 * check box to upload to imgur		NYI
 * text field to enter save destination (maybe a browse button) DONE \ INCOMPLETE
 * Frames per second (for the GIF)	DONE
 * GIF Width 	INCOMPLETE
 * GIF Height 	INCOMPLETE
 * Number of iterations (Maybe want to do "x = y to z", right now we're doing "x=1 to 100") 	INCOMPLETE
 * x axis min and max and name (if we can even set these)	DONE
 * y axis min and max and name (if we can even set these)	DONE
 */

package cgmmdk.fairportAPCompSci.GUI;

import info.monitorenter.gui.chart.Chart2D;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.imageio.stream.FileImageOutputStream;
import javax.imageio.stream.ImageOutputStream;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import cgmmdk.fairportAPCompSci.EquationMaths.Equation;
import cgmmdk.fairportAPCompSci.GIF.GifSequenceWriter;

public class GUI extends JFrame {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;
	private String equation;
	private double xMin;
	private double xMax;
	private double yMin;
	private double yMax;
	private String saveDestination;
	private int fps;

	/**
	 * Create the frame.
	 */
	public GUI() {
		//set up the GUI
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 450, 300);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);
        
        JButton btnEnter = new JButton("Enter");
        btnEnter.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) { 
                //CALL THE METHOD TO CREATE THE GIF   
                
                //saves all of the information from the GUI into variables 
                equation = textField.getText();  
                saveDestination = textField_5.getText(); 
                
                //try/ catch statements to ensure the variables entered are integers
                try{ 
                    xMin = Double.parseDouble(textField_1.getText());  
                }
                catch(NumberFormatException nfe){ 
                    JOptionPane.showMessageDialog(null, "Please enter a numerical value for x min"); 
                    return;
                } 
                try{ 
                    xMax = Double.parseDouble(textField_2.getText());
                }
                catch(NumberFormatException nfe){ 
                    JOptionPane.showMessageDialog(null, "Please enter a numerical value for x max");
                    return;
                } 
                try{ 
                    yMin = Double.parseDouble(textField_3.getText());
                } 
                catch(NumberFormatException nfe){ 
                    JOptionPane.showMessageDialog(null, "Please enter a numerical value for y min");
                    return;
                } 
                try{ 
                    yMax = Double.parseDouble(textField_4.getText()); 
                } 
                catch(NumberFormatException nfe){ 
                    JOptionPane.showMessageDialog(null, "Please enter a numerical value for y max");
                    return;
                }      
                try{ 
                    fps = Integer.parseInt(textField_6.getText());
                }
                catch(NumberFormatException nfe){ 
                    JOptionPane.showMessageDialog(null, "Please enter an integer value for frames per second");
                    return;
				}
				saveGif();
			}
		});
		btnEnter.setBounds(158, 228, 89, 23);
		contentPane.add(btnEnter);

		//label for the equation
        JLabel lblPleaseEnterYour = new JLabel("Please enter your equation:");
        lblPleaseEnterYour.setBounds(19, 21, 172, 14);
        contentPane.add(lblPleaseEnterYour);
        

        //labels
        JLabel lblXValues = new JLabel("X values");
        lblXValues.setBounds(38, 70, 86, 14);
        contentPane.add(lblXValues);
        
        JLabel lblYValues = new JLabel("Y values");
        lblYValues.setBounds(289, 70, 86, 14);
        contentPane.add(lblYValues);
        
        //labels for x min and x max
        JLabel lblMin = new JLabel("Min:");
        lblMin.setBounds(10, 98, 46, 14);
        contentPane.add(lblMin);
        
        JLabel lblMax = new JLabel("Max:");
        lblMax.setBounds(10, 144, 46, 14);
        contentPane.add(lblMax);
        
        //labels for y min and y max
        JLabel lblMin_1 = new JLabel("Min:");
        lblMin_1.setBounds(247, 98, 46, 14);
        contentPane.add(lblMin_1);
        
        JLabel lblMax_1 = new JLabel("Max:");
        lblMax_1.setBounds(247, 144, 46, 14);
        contentPane.add(lblMax_1);
        
        //text field for equation
        textField = new JTextField();
        textField.setBounds(201, 18, 223, 20);
        contentPane.add(textField);
        textField.setColumns(10);
        
        //text field for x min
        textField_1 = new JTextField();
        textField_1.setBounds(48, 95, 86, 20);
        contentPane.add(textField_1);
        textField_1.setColumns(10);
         
        //text field for x max
        textField_2 = new JTextField();
        textField_2.setBounds(48, 141, 86, 20);
        contentPane.add(textField_2);
        textField_2.setColumns(10);
        
        //text field for y min
        textField_3 = new JTextField();
        textField_3.setBounds(289, 95, 86, 20);
        contentPane.add(textField_3);
        textField_3.setColumns(10);
        
        //text field for y max
        textField_4 = new JTextField();
        textField_4.setBounds(289, 141, 86, 20);
        contentPane.add(textField_4);
        textField_4.setColumns(10);
        
        //label for save destination 
        JLabel lblSaveDestination = new JLabel("Save destination:");
        lblSaveDestination.setBounds(10, 202, 114, 14);
        contentPane.add(lblSaveDestination);
        
        
        // text field for save destination
        textField_5 = new JTextField();
        textField_5.setBounds(134, 199, 290, 20);
        contentPane.add(textField_5);
        textField_5.setColumns(10);
        
        //label for frames per second
        JLabel lblFramesPerSecond = new JLabel("Frames per Second:");
        lblFramesPerSecond.setBounds(10, 169, 146, 14);
        contentPane.add(lblFramesPerSecond);
        
        //text field for frames per second
        textField_6 = new JTextField();
        textField_6.setBounds(134, 172, 86, 20);
        contentPane.add(textField_6);
        textField_6.setColumns(10);
	}
	
	/**
	 * Creates the full Saved GIF
	 */
	private void saveGif()
	{
		Equation eq = new Equation(equation, 100,xMin,xMax,yMin,yMax);
		int start = 0, end = 0;
		try {
			// Gets the range from the equation
			System.out.println(equation);
			
			String range = equation.substring(equation.indexOf('['), equation.indexOf(']')+1); // get [a,b] from equationString
			System.out.println("range set to: " + range);
			
			start = Integer.parseInt(range.substring(range.indexOf('[') + 1, range.indexOf(',')));
			System.out.println("start set to: " + start);
			
			end = Integer.parseInt(range.substring(range.indexOf(',')+1, range.indexOf(']')));
			System.out.println("end set to: " + end);
			
		} catch (Exception e) {
		}
		try {
			ImageOutputStream imageIO = new FileImageOutputStream(new File(saveDestination));
			System.out.println(saveDestination);
			
			int timeBetweenFramesMS = 100 / fps;

			GifSequenceWriter gif = new GifSequenceWriter(imageIO,
					(new Chart2D()).snapShot().getType(), timeBetweenFramesMS,
					true);
			// Loops through all the range of values found above and creates the gif.
			for (int i = start; i <= end; i++) {
				gif.writeToSequence(eq.getChart(i).snapShot());
				System.out.println("Interation " + i + " done");
			}
			System.out.println("Fin");
			imageIO.close();
		} catch (Exception e) {
		}
	}
}