package cgmmdk.fairportAPCompSci.main;

import cgmmdk.fairportAPCompSci.GUI.GUI;

public class Driver {

	public static void main(String[] args) {
		try {
			GUI frame = new GUI();
			frame.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
