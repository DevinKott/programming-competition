/* This class stores an equation object. 
 * This is be able to read in something like "[1,10]*Math.pow(x,2) + 10" 
 * and output a chart that satisfy the input String. 
 * The ITERATIONS refers to the values x will cycle through.
 */
package cgmmdk.fairportAPCompSci.EquationMaths;

import info.monitorenter.gui.chart.Chart2D;
import info.monitorenter.gui.chart.IRangePolicy;
import info.monitorenter.gui.chart.rangepolicies.RangePolicyFixedViewport;
import info.monitorenter.gui.chart.traces.Trace2DSimple;
import info.monitorenter.util.Range;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class Equation {

	private final String EQUATION;
	private final int ITERATIONS;
	private final double ITERATOR;
	
	private final double X_MIN, X_MAX, Y_MIN, Y_MAX;
	
	public Equation(final String equation, final int iterations, double xMin, double xMax, double yMin, double yMax) {
		EQUATION = equation;
		ITERATIONS = iterations;
		X_MIN=xMin;
		X_MAX=xMax;
		Y_MIN=yMin;
		Y_MAX=yMax;
		ITERATOR=(X_MAX-X_MIN)*1.0/ITERATIONS;
	}
	
	public Chart2D getChart(int yValue) throws ScriptException{
		Trace2DSimple trace = new Trace2DSimple();
		Chart2D chart = new Chart2D();
		chart.addTrace(trace);
		double x = 0;
		for (double i = 0; i <= ITERATIONS; i+=1) {
			x+=ITERATOR;
			double y = getEquationIteration(x, yValue);
//			System.out.println(x + ", " + y);
			trace.addPoint(x, y);
		}
		// Set the bounds of the axes
		IRangePolicy rangePolicyX = new RangePolicyFixedViewport(new Range(X_MIN, X_MAX));
		IRangePolicy rangePolicyY = new RangePolicyFixedViewport(new Range(Y_MIN, Y_MAX));
		chart.getAxesXBottom().get(0).setRangePolicy(rangePolicyX);
		chart.getAxesYLeft().get(0).setRangePolicy(rangePolicyY);
		
		return chart;
	}
	
	private double getEquationIteration(double xValue, double yValue) throws ScriptException {
		ScriptEngine engine = new ScriptEngineManager().getEngineByName("JavaScript");
		String xReplaced = EQUATION.replaceAll("x", Double.toString(xValue));
		// System.out.println(xReplaced);
		
		String brackets = EQUATION.substring(EQUATION.indexOf('['), EQUATION.indexOf(']') + 1);
		// System.out.println(brackets);
		
		String finalString = xReplaced.replace(brackets, Double.toString(yValue));
		// System.out.println(finalString);
		
		return (double) engine.eval(finalString);
	}

	public final String getEquation() {
		return EQUATION;
	}
}
