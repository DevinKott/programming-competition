# README #

### What is this repository for? ###

* This Program allows users to create simple GIFs when given an equation written in according to JavaSript conventions.
* Version 1.0

### How do I use this? ###

* Once you have this program running, as decribed in the instruction.txt file, follow this guide to use this program.
* There are seven fields to enter in this program. All fields must be filled.
NOTE: The program does check for some error, but be sure look over your values before pressing enter.
* The equation field accepts modified JavaScript equations, see the examples below for more information.
* The x min, y min, x max, and y max fields all accept double values.
* The Frames per Second field accepts an interger value.
NOTE: Many browsers do not follow the proper rules for displaying GIFs at their set speeds, especially at higher fps.
* The Save Destination field accepts a file path for example: 
1. C:\Users\user\Documents\example.gif
2. NewExample.gif // This Will store NewExample.gif in the same folder as the executable.
* Once you have verifies that all answers have been written correctly, click enter.
NOTE: This is a computationally intensive task, and for larger ranges, it could take several minutes.
We reccommend for your first equation, have a range of no larger that 10.
* If the program appears to be unresonsive, it may have to be closed with the task manager. The program may do this if a field has an unchecked, invalid entry.

### Equation Writing ###

As described above, the equation field accepts a modified JavaScript equation, meaning it accepts methods suchas
Those contained in Math. ie. Math.pow(), Math.sqrt(), etc.

The modified part is that the user must also include one range to iterate through, and an x, with will iterate up to xMax.
Ranges are written in the form of [start,end]. For example: [1,10]*x is a valid equation.
Here are some values that produce interesting results (images are included with zip)(format=eq, X(min,max), Y(min,max), fps):
1. Math.sin(x)*[1,100], X(-5,5), Y(-105,105), fps 5
2. Math.abs(Math.sin(x)*[1,100]), X(-5,5), Y(-5,105), fps 5
3. Math.log(Math.sin(x)*[1,100]), X(-5,5), Y(-5,105), fps 5
4. Math.cos(x)*[1,100]*Math.random(), X(-10,10), Y(-105,105), fps 4
5. (This one took a long time ~10min) [-500,500]*x-1/Math.pow(x,2), X(-5,5), Y(-100,100), fps 5

NOTE: View the images with an internet browser for best results. DO NOT open the images until they have finished being written.

### Who do I talk to With Problems? ###

* Chris Grace (Gracecr on BitBucket), or Mike Mallow